-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2021 at 09:48 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tbl_akademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `kode_dosen` varchar(20) NOT NULL,
  `nama_dosen` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(100) NOT NULL,
  `kode_pos` char(5) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `agama` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`kode_dosen`, `nama_dosen`, `alamat`, `kota`, `kode_pos`, `telepon`, `agama`) VALUES
('D001', 'DEDY TRISTANTO', 'HALAN A', 'JAKARTA', '14230', '08932', NULL),
('D002', 'TRIANA FATMAWATI', 'JALAN E', 'MALANG', '', '089289832', NULL),
('D003', 'NOVERIZA', 'JALAN C', 'BANDUNG', '', '2121', NULL),
('D004', 'LUCKY HERMANTO', 'JALAN D', 'PALEMBANG', '', '2111', NULL),
('D005', 'JACOB SARAGIH', 'JALAN E', 'JAKARTA', '', '3293278', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `npm` char(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(50) NOT NULL,
  `kode_pos` char(5) NOT NULL,
  `telepon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`npm`, `nama`, `tgl_lahir`, `agama`, `alamat`, `kota`, `kode_pos`, `telepon`) VALUES
('1312001', 'NUR WAHYUDI', '2021-01-03', 1, 'JALAN B', 'JAKARTA', '14230', '08121013'),
('1312002', 'EBI HANZA', '2021-01-04', 1, 'JALAN C', 'JAKARTA', '', '038221'),
('1312003', 'FITRI CHAERANI', '2021-01-03', 1, 'JALAN D', 'BANDUNG', '', '32981'),
('1312004', 'MUHAMMAD NUR FAYADH', '2021-01-03', 1, 'JALAN E', 'JAKARTA', '14210', '329782');

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE `matakuliah` (
  `kode_mk` varchar(20) NOT NULL,
  `nama_mk` varchar(50) NOT NULL,
  `sks` int(11) NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`kode_mk`, `nama_mk`, `sks`, `status`) VALUES
('TI216', 'MATEMATIKA', 1, '1'),
('TI217', 'BAHASA INDONESIA', 1, '1'),
('TI218', 'RPL', 3, '1'),
('TI219', 'PERANCANGAN SISTEM', 2, '1'),
('TI220', 'TEKNOLOGI INFORMASI DAN JARINGAN', 3, '1'),
('TI221', 'DATABASE', 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `npm` char(10) NOT NULL,
  `kode_dosen` varchar(100) NOT NULL,
  `kode_mk` varchar(20) NOT NULL,
  `nil_mid` decimal(10,0) NOT NULL,
  `nil_fin` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`npm`, `kode_dosen`, `kode_mk`, `nil_mid`, `nil_fin`) VALUES
('1312001', 'D003', 'TI217', '70', '80'),
('1312002', 'D002', 'TI218', '50', '40'),
('1312003', 'D001', 'TI221', '80', '80'),
('1312001', 'D001', 'TI219', '70', '80');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`kode_dosen`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`npm`);

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD PRIMARY KEY (`kode_mk`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD KEY `kode_dosen` (`kode_dosen`),
  ADD KEY `kode_mk` (`kode_mk`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD CONSTRAINT `prestasi_ibfk_1` FOREIGN KEY (`kode_dosen`) REFERENCES `dosen` (`kode_dosen`),
  ADD CONSTRAINT `prestasi_ibfk_2` FOREIGN KEY (`kode_mk`) REFERENCES `matakuliah` (`kode_mk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
